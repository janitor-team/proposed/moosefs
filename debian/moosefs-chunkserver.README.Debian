-------- -------- --------

 Beware of "mlocate" scanning of chunkserver volumes. To avoid problems add
 "/var/lib/mfs" and all volumes as per "/etc/moosefs/mfshdd.cfg" to
 PRUNEPATHS in the "/etc/updatedb.conf" like in the following example:

    PRUNEPATHS="/tmp /var/spool /media /mnt /var/lib/moosefs"

-------- -------- --------

 Make sure that HDD mount points are not writable to "mfs" user to
 avoid using empty mount point as storage when (failed) HDD is not mounted.
 Alternatively storage volume can be configured to use subfolder in
 "/etc/mfs/mfshdd.cfg".

-------- -------- --------

 Chunkserver may be very slow when one of its volumes becomes near-full
 with over 99% utilisation. To relieve situation consider using the
 following setting in the "/etc/mfs/mfschunkserver.cfg":

    HDD_LEAVE_SPACE_DEFAULT = 5G

 This will prevent writes to near full volumes by marking them as 100%
 utilised when they have less than 5 GiB of free space left.

-------- -------- --------

 mfshdd.cfg

 Chunkserver unconditionally initialises given paths with its folder
 structures hence to avoid accidentally using empty mount point as storage
 (e.g. when HDD is not mounted) it is recommended to create subfolders
 on the designated HDDs like in the following examples where "/mnt/hdN"
 represent mount points:

#/mnt/hd3/moosefs
#/mnt/hd3/ext4

-------- -------- --------
