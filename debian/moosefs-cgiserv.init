#!/bin/sh
#
### BEGIN INIT INFO
# Provides:       moosefs-cgiserv
# Required-Start: $local_fs $network $syslog $remote_fs
# Required-Stop:  $local_fs $syslog $remote_fs
# Should-Start:   $syslog
# Should-Stop:    $network $syslog
# Default-Start:  2 3 4 5
# Default-Stop:   0 1 6
# Short-Description: HTTP server for MooseFS CGI monitor
# Description: moosefs-cgiserv daemon is an HTTP server for MooseFS CGI monitor
### END INIT INFO

NAME=moosefs-cgiserv
EXEC=/usr/bin/python
DAEMON=/usr/sbin/mfscgiserv
MFSCGISERV_USER=mfs
MFSCGISERV_GROUP=nogroup
BIND_HOST=0.0.0.0
BIND_PORT=9425
ROOT_PATH=/usr/share/moosefs-cgi

# Exit if executable is not installed
[ -x $DAEMON ] || exit 0

# Read configuration variable file if it is present
DEFAULTS_FILE=/etc/default/${NAME}
[ -r "$DEFAULTS_FILE" ] && . $DEFAULTS_FILE

PIDF=/var/run/${NAME}.pid
RETRY=TERM/30/KILL/5

# Load the VERBOSE setting and other rcS variables
[ -f /etc/default/rcS ] && . /etc/default/rcS

# define LSB log_* functions.
. /lib/lsb/init-functions

case "$1" in
    start)
        if $0 status >>/dev/null; then
            log_action_msg "$NAME is already running"
            exit 0
        fi
        log_action_begin_msg "$NAME starting"
        if R=$(start-stop-daemon --start --exec ${EXEC} --oknodo --pidfile ${PIDF} --make-pidfile \
            --background --chuid ${MFSCGISERV_USER}:${MFSCGISERV_GROUP} \
            -- $DAEMON -f -H ${BIND_HOST} -P ${BIND_PORT} -R ${ROOT_PATH} 2>&1);
        then
            log_action_end_msg 0 "$R"
        else
            log_action_end_msg 1 "$R"
        fi
    ;;
    stop)
        log_action_begin_msg "$NAME stopping"
        if R=$(start-stop-daemon --stop --exec ${EXEC} --pidfile ${PIDF} --remove-pidfile \
            --retry=$RETRY --quiet);
        then
            log_action_end_msg 0 "$R"
        else
            log_action_end_msg 1 "not running"
        fi
    ;;
    force-reload|restart)
        $0 stop
        $0 start
    ;;
    status)
        ## return status 0 if process is running.
        status_of_proc -p ${PIDF} "$DAEMON" "$NAME"
    ;;
    *)
        N=/etc/init.d/$NAME
        echo "Usage: $N {start|stop|restart|force-reload|status}" >&2
    ;;
esac
